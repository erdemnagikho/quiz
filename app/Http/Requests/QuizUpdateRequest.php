<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuizUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|min:3|max:250',
            'description' => 'max:1000',
            'finished_at' => 'nullable|after:'.now()
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Quiz Başlığı',
            'description' => 'Quiz Açıklama',
            'finished_at' => 'Quiz Bitiş Tarihi'
        ];
    }
}
