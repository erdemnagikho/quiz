<x-app-layout>
    <x-slot name="header">Quiz Oluştur</x-slot>
    <div class="card">
        <div class="card-body">
            <form method="post" action="{{route('quizzes.store')}}">
                @csrf
                <div class="form-group">
                    <label for="">Quiz Başlık</label>
                    <input type="text" name="title" class="form-control" value="{{old('title')}}">
                </div>
                <div class="form-group">
                    <label for="">Quiz Açıklama</label>
                    <textarea name="description" class="form-control" rows="4">
                        {{old('description')}}
                    </textarea>
                </div>
                <div class="form-group">
                    <input id="hasFinishedDate" @if(old('finished_at')) checked @endif type="checkbox">
                    <label for="">Görüntüleme İçin Son Tarih Olacak Mı?</label>
                </div>
                <div id="finishedDate" class="form-group"  @if(!old('finished_at'))style="display: none"@endif>
                    <label for="">Görüntüleme İçin Son Tarih</label>
                    <input type="datetime-local" value="{{old('finished_at')}}" name="finished_at" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block">Quiz Oluştur</button>
                </div>
            </form>
        </div>
    </div>
    <x-slot name="script">
        <script>
            $("#hasFinishedDate").change(function () {
                if($("#hasFinishedDate").is(":checked")) {
                    $("#finishedDate").show();
                }else {
                    $("#finishedDate").hide();
                }
            });
        </script>
    </x-slot>
</x-app-layout>
