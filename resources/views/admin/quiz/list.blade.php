<x-app-layout>
    <x-slot name="header">Quizler</x-slot>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">
                <a href="{{route('quizzes.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Quiz Oluştur</a>
            </h5>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Quiz</td>
                        <td>Durum</td>
                        <td>Bitiş Tarihi</td>
                        <td>İşlemler</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($quizzes as $quiz)
                        <tr>
                            <th>{{$quiz->title}}</th>
                            <td>{{$quiz->status}}</td>
                            <td>{{$quiz->finished_at}}</td>
                            <td>
                                <a href="{{route('quizzes.edit', $quiz->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-pen mr-1"></i> Düzenle</a>
                                <a href="{{route('quizzes.destroy', $quiz->id)}}" class="btn btn-sm btn-danger"><i class="fa fa-trash mr-1"></i> Sil</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$quizzes->links()}}
        </div>
    </div>
</x-app-layout>
