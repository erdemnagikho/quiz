<x-app-layout>
    <x-slot name="header">Quiz Güncelle</x-slot>
    <div class="card">
        <div class="card-body">
            <form method="post" action="{{route('quizzes.update', $quiz->id)}}">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="">Quiz Başlık</label>
                    <input type="text" name="title" class="form-control" value="{{$quiz->title}}">
                </div>
                <div class="form-group">
                    <label for="">Quiz Açıklama</label>
                    <textarea name="description" class="form-control" rows="4">{{$quiz->description}}</textarea>
                </div>
                <div class="form-group">
                    <input id="hasFinishedDate" @if($quiz->finished_at) checked @endif type="checkbox">
                    <label for="">Görüntüleme İçin Son Tarih Olacak Mı?</label>
                </div>
                <div id="finishedDate" class="form-group" @if(!$quiz->finished_at)style="display: none"@endif>
                    <label for="">Görüntüleme İçin Son Tarih</label>
                    <input type="datetime-local"
                           @if($quiz->finished_at) value="{{ date('Y-m-d\TH:i', strtotime($quiz->finished_at)) }}"
                           @endif name="finished_at" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block">Quiz Güncelle</button>
                </div>
            </form>
        </div>
    </div>
    <x-slot name="script">
        <script>
            $("#hasFinishedDate").change(function () {
                if ($("#hasFinishedDate").is(":checked")) {
                    $("#finishedDate").show();
                } else {
                    $("#finishedDate").hide();
                }
            });
        </script>
    </x-slot>
</x-app-layout>
